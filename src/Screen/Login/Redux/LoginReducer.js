import {
  LOGIN_SUCCESS,
  LOGOUT,
  LOGIN_FAILED,
  SET_USER_DATA,
} from '../Redux/ActionLogin';

import {SET_PROFILE_SUCCESS} from '../../Profile/Redux/ActionProfile';

const initialState = {
  data: [],
  message: '',
  userData: [],
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case LOGIN_SUCCESS:
      return {
        ...state,
        data: payload,
      };
    case LOGIN_FAILED:
      return {
        ...state,
        message: payload,
      };
    case SET_USER_DATA:
      return {
        ...state,
        userData: payload,
      };
    case SET_PROFILE_SUCCESS:
      return {
        ...state,
        userData: payload.result_updated,
        message: payload.message,
      };

    case LOGOUT:
      return {...state, data: [], userData: []};
    default:
      return state;
  }
};
