export const GET_CATEGORY = 'GET_CATEGORY';
export const SET_CATEGORY = 'SET_CATEGORY';
export const GET_CATEGORY_FAILED = 'GET_CATEGORY_FAILED';
export const GET_DATA_BY_MONTH = 'GET_DATA_BY_MONTH';
export const SET_DATA_BY_MONTH = 'SET_DATA_BY_MONTH';

export const setDataHome = payload => {
  return {
    type: SET_CATEGORY,
    payload,
  };
};

export const getDataHome = payload => {
  return {
    type: GET_CATEGORY,
    payload,
  };
};

export const getDataHomeFailed = payload => ({
  type: GET_CATEGORY_FAILED,
  payload,
});

export const getDataByMonth = payload => ({type: GET_DATA_BY_MONTH, payload});

export const setDataByMonth = payload => ({type: SET_DATA_BY_MONTH, payload});
