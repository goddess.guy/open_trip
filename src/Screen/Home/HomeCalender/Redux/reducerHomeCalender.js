import {SET_HOME_CALENDER} from './actionHomeCalender';

const initialState = {
  data: [],
};

const reducerHomeCalender = (state = initialState, action) => {
  switch (action.type) {
    case SET_HOME_CALENDER:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default reducerHomeCalender;
