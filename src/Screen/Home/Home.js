import React, {useEffect, useState, useCallback} from 'react';
import {
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Text,
  RefreshControl,
} from 'react-native';
import MySearchBar from '../../Components/SearchBar';
import FastImage from 'react-native-fast-image';
import Feather from 'react-native-vector-icons/Feather';
import {Snackbar} from 'react-native-paper';
// component
import styles from './style';
import HomeButton from '../../Components/HomeButton';
import {DUMMY} from '../../Utils/DummyData';
import {getDataByMonth, getDataHome} from './Redux/ActionHome';
import {getHomeCalender} from '../Home/HomeCalender/Redux/actionHomeCalender';
import Inter from '../../Components/Inter';
// redux
import {useDispatch, useSelector} from 'react-redux';
import LoadingAnimated from '../../Components/Loading';
import {
  actionSuccess,
  actionExpired,
  actionIsLogged,
} from '../../Redux/GlobalAction';
import {getDetail} from './HomeDetail/Redux/ActionHomeDetail';
import jwt_decode from 'jwt-decode';
import {setUserData} from '../Login/Redux/ActionLogin';
import {navigate} from '../../Function/nav';
import moment from 'moment';
import ErrorScreen from '../../Components/Error';

const Home = props => {
  // const DataHome = useSelector(state => {
  //   if (state.ReducerHomePage.data.length > 0) {
  //     return state.ReducerHomePage.data;
  //   } else {
  //     return [];
  //   }
  // });
  const [pemesanData, setPemesanData] = useState();

  // list button
  const listButton = [
    {name: 'Kalender', type: 'kalender'},
    {name: moment().add(1, 'months').format('MMMM YYYY'), type: 1},
    {name: moment().add(2, 'months').format('MMMM YYYY'), type: 2},
    {name: moment().add(3, 'months').format('MMMM YYYY'), type: 3},
  ];
  const coba = moment('September 2021').startOf('month').format('DD MM YYYY');
  console.log(coba, 'cobain guys');

  // Dummy API
  // const dataTrip = DUMMY[0].data;
  // const namaTrip = DUMMY;

  const dispatch = useDispatch();

  const DataCategory = useSelector(state => state.ReducerHomePage.data);
  const nameMonth = useSelector(state => state.ReducerHomePage?.monthName);
  const nameDate = useSelector(state => state.ReducerHomePage?.dateName);
  const LoadingData = useSelector(state => state.Global.loading);
  const token = useSelector(state => state.Login?.data.token_traveller);
  const Logged = useSelector(state => state.Global.isLogged);
  const success = useSelector(state => state.Global.Success);
  const TimeExp = useSelector(state => state.Global?.timeExp);

  // console.log(DataCategory, ' INI CATEGORY');

  const [refres, setrefres] = useState(false);
  const wait = timeout => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  };
  const Refresh = useCallback(() => {
    setrefres(true);
    dispatch(getDataHome());
    wait(2000).then(() => setrefres(false));
  }, []);

  useEffect(() => {
    // if (moment().format('LTS') >= TimeExp) {
    //   dispatch(actionExpired(true));
    //   dispatch(actionIsLogged(false));
    //   navigate('LoginScreen');
    // }
    if (Logged) {
      const decoded = jwt_decode(token);
      setPemesanData(decoded);
      dispatch(setUserData(decoded));
      dispatch(getDataHome());
    }
  }, [dispatch]);

  return (
    <SafeAreaView style={styles.fullScreen}>
      {/* SEARCH BAR */}
      <View style={styles.container}>
        <MySearchBar whiteStyle name="where do you want to go?" />
      </View>

      <View style={styles.backgroundHome}>
        {/* BUTTON */}
        <View style={styles.buttonContainer}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.button}>
              {listButton.map((e, i) => {
                return (
                  <HomeButton
                    key={i}
                    warna={'#f26683'}
                    name={e.name}
                    kalender={() => {
                      // props.navigation.navigate('Calendar');
                      const date = moment()
                        .add(e.type, 'month')
                        .format('DD/MM/YYYY');
                      if (e.type === 'kalender') {
                        props.navigation.navigate('Calendar');
                      } else {
                        dispatch(
                          getDataByMonth({
                            start: moment(date, 'DD/MM/YYYY')
                              .startOf('month')
                              .format('DD/MM/YYYY'),
                            end: moment(date, 'DD/MM/YYYY')
                              .endOf('month')
                              .format('DD/MM/YYYY'),
                            monthName: e.name,
                          }),
                        );
                      }
                    }}
                  />
                );
              })}
            </View>
          </ScrollView>
        </View>

        {/* HOME DESC */}
        {LoadingData ? (
          <LoadingAnimated />
        ) : (
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={refres} onRefresh={Refresh} />
            }
            style={styles.scroll}>
            {DataCategory.length !== 4 ? (
              <View style={styles.card}>
                <View style={styles.textBymMonthContainer}>
                  <Inter
                    type="Bold"
                    text={`List Of ${
                      nameDate ? nameDate : nameMonth ? nameMonth : ''
                    }`}
                  />
                </View>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={DataCategory}
                  keyExtractor={items => items.id}
                  renderItem={({item, index}) => (
                    <>
                      <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => dispatch(getDetail(item.id))}>
                        <FastImage
                          resizeMode={FastImage.resizeMode.cover}
                          source={{
                            uri: item.thumbnail_pict,
                            priority: FastImage.priority.high,
                          }}
                          style={styles.cardImage}>
                          <View style={styles.cardContainer}>
                            <Inter
                              color="white"
                              type="Bold"
                              style={styles.placeName}
                              text={item.trip_name}
                            />
                          </View>

                          <Feather
                            name="map-pin"
                            size={16}
                            color="white"
                            style={styles.iconMap}
                          />
                          <Inter style={styles.locName} text={item.pick_spot} />

                          <Inter
                            style={styles.durTrip}
                            text={`${item.duration_trip}Days`}
                          />
                        </FastImage>
                      </TouchableOpacity>
                    </>
                  )}
                />
              </View>
            ) : DataCategory.length < 4 ? (
              <View style={styles.card}>
                <View style={styles.textBymMonthContainer}>
                  <Inter
                    type="Bold"
                    text={`List Of ${
                      nameDate ? nameDate : nameMonth ? nameMonth : ''
                    }`}
                  />
                </View>
                <FlatList
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                  data={DataCategory}
                  keyExtractor={items => items.id}
                  renderItem={({item, index}) => (
                    <>
                      <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => dispatch(getDetail(item.id))}>
                        <FastImage
                          resizeMode={FastImage.resizeMode.cover}
                          source={{
                            uri: item.thumbnail_pict,
                            priority: FastImage.priority.high,
                          }}
                          style={styles.cardImage}>
                          <View style={styles.cardContainer}>
                            <Inter
                              color="white"
                              type="Bold"
                              style={styles.placeName}
                              text={item.trip_name}
                            />
                          </View>

                          <Feather
                            name="map-pin"
                            size={16}
                            color="white"
                            style={styles.iconMap}
                          />
                          <Inter style={styles.locName} text={item.pick_spot} />

                          <Inter
                            style={styles.durTrip}
                            text={`${item.duration_trip}Days`}
                          />
                        </FastImage>
                      </TouchableOpacity>
                    </>
                  )}
                />
              </View>
            ) : DataCategory.length !== 0 ? (
              <>
                {DataCategory.map((e, i) => {
                  return (
                    <View key={i}>
                      <View style={styles.homeDesc}>
                        <TouchableOpacity>
                          <Text style={styles.desc}>{e.category_name}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={
                            () => dispatch(getHomeCalender(e.id))
                            // props.navigation.navigate(
                            //   'HomeCalender',
                            //   e.category_name,
                            // )
                          }>
                          <Inter style={styles.descSeeMore} text="See More" />
                        </TouchableOpacity>
                      </View>

                      {/* {DataCategory.map((e, i) => {
                    return ( */}
                      <View key={i} style={styles.card}>
                        <FlatList
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}
                          data={e.Trips}
                          keyExtractor={items => items.id}
                          renderItem={({item, index}) => (
                            <>
                              <TouchableOpacity
                                activeOpacity={0.5}
                                onPress={() => dispatch(getDetail(item.id))}>
                                <FastImage
                                  resizeMode={FastImage.resizeMode.cover}
                                  source={{
                                    uri: item.thumbnail_pict,
                                    priority: FastImage.priority.high,
                                  }}
                                  style={styles.cardImage}>
                                  <View style={styles.cardContainer}>
                                    <Inter
                                      color="white"
                                      type="Bold"
                                      style={styles.placeName}
                                      text={item.trip_name}
                                    />
                                  </View>

                                  <Feather
                                    name="map-pin"
                                    size={16}
                                    color="white"
                                    style={styles.iconMap}
                                  />
                                  <Inter
                                    style={styles.locName}
                                    text={item.pick_spot}
                                  />

                                  <Inter
                                    style={styles.durTrip}
                                    text={`${item.duration_trip}Days`}
                                  />
                                </FastImage>
                              </TouchableOpacity>
                            </>
                          )}
                        />
                      </View>
                      {/* );
                  })} */}
                    </View>
                  );
                })}
              </>
            ) : (
              <ErrorScreen />
            )}
          </ScrollView>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Home;
