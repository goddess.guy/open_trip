import {GET_DETAIL_ERROR, GET_DETAIL_SUCCESS} from './ActionHomeDetail';

const initialState = {
  detail: [],
  message: '',
};

const reducerDetailHome = (state = initialState, {type, payload}) => {
  switch (type) {
    case GET_DETAIL_SUCCESS:
      return {...state, detail: payload};

    case GET_DETAIL_ERROR:
      return {...state, message: payload};

    default:
      return state;
  }
};

export default reducerDetailHome;
