import React, {useEffect, useState} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  LogBox,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Inter from '../../../Components/Inter';
import {COLORS} from '../../../Utils/Color';

import {Chip, Button, Snackbar} from 'react-native-paper';

import ClipBoard from '@react-native-clipboard/clipboard';
import ErrorScreen from '../../../Components/Error';

// import Bca from '../../../Assets/Images/bca.svg';
import Atm from '../../../Assets/Images/atm.svg';
import AtmNon from '../../../Assets/Images/atmnon.svg';
import Ibank from '../../../Assets/Images/internet-banking.svg';
import IbankNon from '../../../Assets/Images/internet-banking-non.svg';
import Mbank from '../../../Assets/Images/mobile-payment.svg';
import MbankNon from '../../../Assets/Images/mobile-payment-non.svg';
import {useDispatch, useSelector} from 'react-redux';
import LoadingAnimated from '../../../Components/Loading';
import jwt_decode from 'jwt-decode';
import moment from 'moment';
// import {listImageBank} from '../../../Assets/Images/BankImage';
import Banks from '../../../Assets/Images/BankImage';

const Top = createMaterialTopTabNavigator();

const OrderReceivedScreen = () => {
  // const [accountBank, setaccountBank] = useState('039109123');

  const Loading = useSelector(state => state.Global.loading);
  const pemesanData = useSelector(state => state.Login.userData);
  // const dataDetail = useSelector(state => state.DetailHome.detail);
  // const token = useSelector(state => state.Login.data.token_traveller);
  const dataOrder = useSelector(state => state.Order?.order);
  const failed = useSelector(state => state.Global.failed);

  // bank
  const dataBank = dataOrder.no_rekening_host.split(' ').slice(0, 1).join('');
  // console.log(dataBank, 'databank kuy');

  // const rekBank = dataOrder.nama_bank_host.split(' ');
  // if (rekBank.length > 1) {
  //   rekBank.slice(1).join('');
  // } else {
  //   rekBank.join('');
  // }
  const bankRek = data => {
    const arrRek = data.split(' ');
    if (arrRek.length > 1) {
      return arrRek.slice(1).join('');
    } else {
      return arrRek.join('');
    }
  };

  // const namaBank = listImageBank.filter(e => e.bank_name.includes(tes));
  // console.log(namaBank);

  const [visible, setvisible] = useState(false);

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  const atm = () => {
    return (
      <View
        style={{
          height: heightPercentageToDP(25),
          paddingVertical: moderateScale(20),
        }}>
        <Inter
          text="Melalui ATM"
          size={moderateScale(14)}
          color={COLORS.red}
          type="Bold"
        />
        <View style={{padding: moderateScale(10)}}>
          <Text style={styles.textPetunjuk}>
            Masukkan kartu ATM dan PIN anda. Pada menu utama, pilih
            <Inter
              text=" Transaksi Lainnya "
              size={moderateScale(12)}
              color={COLORS.red}
            />
          </Text>
          <Text style={styles.textPetunjuk}>
            Pilih "
            <Inter
              text="Transfer"
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              kemudian tuliskan "
              <Inter
                text="rekening diatas"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Masukkan nomor "
            <Inter
              text={dataBank}
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pilih "
              <Inter text="Benar" size={moderateScale(12)} color={COLORS.red} />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Periksa rincian pembayaran anda. Jika benar pilih "
            <Inter text="Ya" size={moderateScale(12)} color={COLORS.red} />"
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pembayaran "
              <Inter
                text="Selesai"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Inter
            text={dataOrder.orders.payment_ways}
            size={moderateScale(12)}
            color={COLORS.red}
            type="Bold"
          />
        </View>
      </View>
    );
  };
  const iBank = () => {
    return (
      <View
        style={{
          height: heightPercentageToDP(30),
          paddingVertical: moderateScale(20),
        }}>
        <Inter
          text="Melalui Internet Banking"
          size={moderateScale(14)}
          color={COLORS.red}
          type="Bold"
        />
        <View style={{padding: moderateScale(10)}}>
          <Text style={styles.textPetunjuk}>
            Login ke Internet Bank Anda. Pada menu utama, pilih
            <Inter
              text=" Transfer "
              size={moderateScale(12)}
              color={COLORS.red}
            />
          </Text>
          <Text style={styles.textPetunjuk}>
            Pilih "
            <Inter
              text="Transfer Antar Bank"
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              kemudian tuliskan "
              <Inter
                text="rekening diatas"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Masukkan nomor "
            <Inter
              text={dataBank}
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pilih "
              <Inter text="Benar" size={moderateScale(12)} color={COLORS.red} />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Periksa rincian pembayaran anda. Jika benar pilih "
            <Inter text="Ya" size={moderateScale(12)} color={COLORS.red} />"
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pembayaran "
              <Inter
                text="Selesai"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Inter
            text={dataOrder.orders.payment_ways}
            size={moderateScale(12)}
            color={COLORS.red}
            type="Bold"
          />
        </View>
      </View>
    );
  };
  const mBank = () => {
    return (
      <View
        style={{
          height: heightPercentageToDP(25),
          paddingVertical: moderateScale(20),
        }}>
        <Inter
          text="Melalui Mobile Banking"
          size={moderateScale(14)}
          color={COLORS.red}
          type="Bold"
        />
        <View style={{padding: moderateScale(10)}}>
          <Text style={styles.textPetunjuk}>
            Login ke aplikasi mobile bank anda. Pada menu utama, pilih
            <Inter
              text=" Transfer "
              size={moderateScale(12)}
              color={COLORS.red}
            />
          </Text>
          <Text style={styles.textPetunjuk}>
            Pilih "
            <Inter
              text="Transfer Antar Bank"
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              kemudian tuliskan "
              <Inter
                text="rekening diatas"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Masukkan nomor "
            <Inter
              text={dataBank}
              size={moderateScale(12)}
              color={COLORS.red}
            />
            "
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pilih "
              <Inter text="Benar" size={moderateScale(12)} color={COLORS.red} />
              "
            </Text>
          </Text>
          <Text style={styles.textPetunjuk}>
            Periksa rincian pembayaran anda. Jika benar pilih "
            <Inter text="Ya" size={moderateScale(12)} color={COLORS.red} />"
            <Text style={styles.textPetunjuk}>
              {' '}
              dan pembayaran "
              <Inter
                text="Selesai"
                size={moderateScale(12)}
                color={COLORS.red}
              />
              "
            </Text>
          </Text>
          <Inter
            text={dataOrder.orders.payment_ways}
            size={moderateScale(12)}
            color={COLORS.red}
            type="Bold"
          />
        </View>
      </View>
    );
  };

  return failed ? (
    <ErrorScreen />
  ) : (
    <SafeAreaView style={styles.safeView}>
      {Loading ? (
        <LoadingAnimated />
      ) : (
        <ScrollView>
          <View
            style={{
              paddingHorizontal: moderateScale(20),
              paddingVertical: moderateScale(20),
            }}>
            {/* received section */}
            <View style={styles.receivedContainer}>
              <View style={styles.receivedStyle}>
                <Ionicons
                  name="checkmark-circle-sharp"
                  size={moderateScale(76)}
                  color={COLORS.red}
                />
                <Inter
                  text="Thank you. Your order has been received"
                  size={moderateScale(20)}
                  type="Bold"
                  style={styles.textCenter}
                />
                {/*  status section */}
                <Chip mode="outlined" style={styles.statusStyle}>
                  <Inter
                    style={styles.opacity1}
                    color={COLORS.yellow}
                    text={dataOrder.orders.payment_status}
                    size={moderateScale(12)}
                  />
                </Chip>
              </View>
            </View>
            {/* detail pemesan */}
            <View style={styles.pemesanContainer}>
              <View style={styles.pemesanStyle}>
                <Inter
                  text="Nama Pemesan"
                  size={moderateScale(12)}
                  type="ExtraLight"
                />
                <Inter text={pemesanData.username} size={moderateScale(12)} />
              </View>
              {/* <View style={styles.pemesanStyle}>
                <Inter
                  text="Nomor Pemesan"
                  size={moderateScale(12)}
                  type="ExtraLight"
                />
                <Inter text="092909812307182" size={moderateScale(12)} />
              </View> */}
              <View style={styles.pemesanStyle}>
                <Inter
                  text="Tanggal"
                  size={moderateScale(12)}
                  type="ExtraLight"
                />
                <Inter
                  text={moment(new Date()).format('DD MMMM YYYY')}
                  size={moderateScale(12)}
                />
              </View>
              <View style={styles.pemesanStyle}>
                <Inter
                  text="Email"
                  size={moderateScale(12)}
                  type="ExtraLight"
                />
                <Inter text={pemesanData.email} size={moderateScale(12)} />
              </View>
              <View style={styles.pemesanStyle}>
                <Inter
                  text="Metode Pembayaran"
                  size={moderateScale(12)}
                  type="ExtraLight"
                />
                <Inter text="Bank Transfer" size={moderateScale(12)} />
              </View>
            </View>

            {/* total pembayaran */}
            <View style={{paddingVertical: moderateScale(10)}}>
              <View style={styles.totalStyle}>
                <Inter
                  text="Total Pembayaran"
                  size={moderateScale(14)}
                  type="Light"
                />
                <Inter
                  text={`Rp. ${new Intl.NumberFormat(['ban', 'id']).format(
                    dataOrder.orders.total_price,
                  )}`}
                  size={moderateScale(14)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>

              {/* Pembayaran section */}
              <View style={styles.totalStyle}>
                <Inter
                  text="Batas Pembayaran"
                  size={moderateScale(14)}
                  type="Light"
                />
                <Inter
                  text={moment(new Date())
                    .add(2, 'days')
                    .format('DD MMMM YYYY')}
                  size={moderateScale(14)}
                  color={COLORS.red}
                  type="Bold"
                />
              </View>
            </View>
          </View>

          {/* Rekening Section */}
          <View style={styles.rekeningContainer}>
            <View style={{paddingBottom: moderateScale(10)}}>
              <Inter
                text="Metode Pembayaran"
                type="Bold"
                size={moderateScale(14)}
              />
            </View>

            <Banks
              file={bankRek(dataOrder.nama_bank_host)}
              width={moderateScale(130)}
              height={moderateScale(50)}
            />

            {/* <Bca width={moderateScale(100)} height={moderateScale(50)} /> */}
            <View style={styles.rekeningStyle}>
              <Inter text="Transfer Bank (Manual)" size={moderateScale(12)} />
              <Inter text="No. Rekening Bank" size={moderateScale(12)} />
            </View>
            <View style={styles.boxNumber}>
              <Inter
                text={dataBank}
                color={COLORS.red}
                size={moderateScale(14)}
              />
            </View>
            <Button
              onPress={() => {
                ClipBoard.setString(dataBank);
                setvisible(!visible);
              }}
              labelStyle={{fontSize: moderateScale(14)}}>
              Salin Nomor
            </Button>
            <Snackbar
              duration={500}
              visible={visible}
              onDismiss={() => setvisible(false)}
              style={{backgroundColor: COLORS.red}}>
              Copied to clipboard
            </Snackbar>
          </View>

          <View
            style={{
              paddingHorizontal: moderateScale(20),
              paddingVertical: moderateScale(20),
            }}>
            <Top.Navigator
              tabBarOptions={{
                activeTintColor: COLORS.red,
                inactiveTintColor: COLORS.softBlack,
                showIcon: true,
                indicatorStyle: {
                  backgroundColor: COLORS.red,
                },
                labelStyle: {
                  fontSize: moderateScale(12),
                  fontFamily: 'Inter',
                  width: widthPercentageToDP(27),
                  fontWeight: 'bold',
                },
                tabStyle: {height: heightPercentageToDP(15)},
                iconStyle: {
                  width: moderateScale(34),
                  height: moderateScale(34),
                },
                style: {elevation: 0},
              }}>
              <Top.Screen
                name="atm"
                component={atm}
                options={{
                  tabBarLabel: 'ATM',
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <Atm
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ) : (
                      <AtmNon
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ),
                }}
              />
              <Top.Screen
                name="ibank"
                component={iBank}
                options={{
                  tabBarLabel: 'Internet Bank',
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <Ibank
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ) : (
                      <IbankNon
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ),
                }}
              />
              <Top.Screen
                name="mbank"
                component={mBank}
                options={{
                  tabBarLabel: 'M-Banking',
                  tabBarIcon: ({focused}) =>
                    focused ? (
                      <Mbank
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ) : (
                      <MbankNon
                        width={moderateScale(34)}
                        height={moderateScale(34)}
                      />
                    ),
                }}
              />
            </Top.Navigator>
          </View>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

export default OrderReceivedScreen;

const styles = StyleSheet.create({
  textPetunjuk: {
    fontSize: moderateScale(12),
    fontFamily: 'Inter',
    color: COLORS.black,
  },
  safeView: {
    flex: 1,
  },
  receivedContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  receivedStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
    width: widthPercentageToDP(60),
  },
  textCenter: {
    textAlign: 'center',
  },
  statusStyle: {
    borderRadius: moderateScale(5),
    borderColor: COLORS.yellow,
    backgroundColor: COLORS.yellowBorder,
    borderWidth: moderateScale(1),
    width: widthPercentageToDP(45),
    height: heightPercentageToDP(6),
    marginTop: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'center',
  },
  opacity1: {
    opacity: 1,
  },
  pemesanContainer: {
    paddingVertical: moderateScale(30),
    borderBottomWidth: moderateScale(1),
    borderBottomColor: COLORS.softBlack,
    borderRadius: moderateScale(1),
    borderStyle: 'dotted',
  },
  pemesanStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: moderateScale(3),
  },
  totalStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: moderateScale(5),
  },
  rekeningContainer: {
    backgroundColor: COLORS.grey,
    width: widthPercentageToDP(100),
    height: heightPercentageToDP(30),
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: moderateScale(50),
  },
  rekeningStyle: {
    paddingBottom: moderateScale(10),
    alignItems: 'center',
    justifyContent: 'center',
  },
  boxNumber: {
    borderWidth: moderateScale(1),
    width: widthPercentageToDP(35),
    height: heightPercentageToDP(5),
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: moderateScale(10),
    borderColor: COLORS.red,
    borderStyle: 'dashed',
  },
});
