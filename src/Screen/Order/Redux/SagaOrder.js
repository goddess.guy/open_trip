import {takeLatest, put} from 'redux-saga/effects';
import {openTrip} from '../../../Utils/baseurl';
import {
  GET_DATA_BANK,
  setDataBankSuccess,
  setDataBankFailed,
  POST_ORDER,
  setOrderSuccess,
  setOrderFailed,
  GET_ORDER_ID,
} from './ActionOrder';
import {
  actionLoading,
  actionSuccess,
  actionFailed,
} from '../../../Redux/GlobalAction';
import {navigate} from '../../../Function/nav';
import {
  setOrderIdSuccess,
  setOrderIdFailed,
} from '../../Profile/Redux/ActionProfile';

function* getDataBank() {
  try {
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: 'bank/all',
      method: 'GET',
      validateStatus: status => status < 505,
    });

    if (res.status === 200) {
      yield put(actionSuccess(true));
      yield put(setDataBankSuccess(res.data.result));
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(setDataBankFailed(error.response.data.message));
    yield put(actionLoading(false));
  }
}

function* postOrder(action) {
  try {
    const {fullname, email, trip_id, qty, bank_id, token} = action.payload;
    const body = {fullname, email, trip_id, qty, bank_id};
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: 'order/crt',
      method: 'POST',
      data: body,
      headers: {Authorization: token},
      validateStatus: status => status < 505,
    });

    console.log(res, 'post order');

    if (res.status === 201) {
      yield put(actionSuccess(true));
      yield put(setOrderSuccess(res.data.result));
      yield navigate('OrderReceived');
      yield put(actionLoading(false));
    }
    if (res.status === 409) {
      yield put(actionFailed(true));
      yield put(setOrderFailed(res.data.message));
      yield navigate('Order');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(setOrderFailed(error.response.data.message));
    yield navigate('HomeDetail');
    yield put(actionLoading(false));
  }
}

function* getOrderId(action) {
  try {
    yield put(actionLoading(true));
    const res = yield openTrip({
      url: `order?id=${action.payload}`,
      method: 'GET',
      validateStatus: status => status < 505,
    });

    if (res.status === 200) {
      yield put(actionSuccess(true));
      yield put(setOrderIdSuccess(res.data.result));
      yield navigate('Voucher');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield put(actionFailed(true));
    yield put(setOrderIdFailed(error.response.data.message));
    yield put(actionLoading(false));
  }
}

export function* SagaOrder() {
  yield takeLatest(GET_DATA_BANK, getDataBank);
  yield takeLatest(POST_ORDER, postOrder);
  yield takeLatest(GET_ORDER_ID, getOrderId);
}
