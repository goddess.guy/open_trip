import {SET_CALENDER} from './actionCalender';

const initialState = {
  data: [],
  message: '',
};

const reducerCalender = (state = initialState, action) => {
  switch (action.type) {
    case SET_CALENDER:
      return {
        ...state,
        data: action.payload,
      };

    default:
      return state;
  }
};

export default reducerCalender;
