export const GET_CALENDER = 'GET_CALENDER';
export const SET_CALENDER = 'SET_CALENDER';
export const SET_DATA_BY_DATE = 'SET_DATA_BY_DATE';

export const setDataCalender = payload => {
  return {
    type: SET_CALENDER,
    payload,
  };
};

export const getDataCalender = payload => {
  return {
    type: GET_CALENDER,
    payload,
  };
};

export const setDataByDate = payload => ({type: SET_DATA_BY_DATE, payload});
