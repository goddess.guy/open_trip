import React, {useState, useEffect} from 'react';
import {View, SafeAreaView} from 'react-native';
import {Calendar} from 'react-native-calendars';
import HeaderTab from '../../Components/Header';
import {COLORS} from '../../Utils/Color';
import {useDispatch, useSelector} from 'react-redux';
import {getDataCalender} from './Redux/actionCalender';
import moment from 'moment';
export default function Calender(props) {
  const [date, setdate] = useState(null);
  console.log(date, 'date cuy');
  const goHome = () => {
    props.navigation.navigate('Home');
  };
  // const selectDate = day => {
  //   let selectedDate = day.dateString;
  //   let newDates = date;
  //   if (date[selectedDate]) {
  //     delete newDates[selectedDate];
  //   } else {
  //     newDates[selectedDate] = {
  //       selected: true,
  //       disableTouchEvent: true,
  //       selectedColor: COLORS.white,
  //       selectedTextColor: COLORS.black,
  //       selectedDotColor: COLORS.red,
  //       marked: true,
  //       activeOpacity: 0,
  //     };
  //   }
  //   setdate({...newDates});
  // };

  const dispatch = useDispatch();
  // const DataCalender = useSelector(state => state.ReducerCalender.data);
  // useEffect(() => {
  //   dispatch(getDataCalender());
  // }, [dispatch]);

  // const [date, setDate] = useState(moment().format('DD-MM-YYYY'));
  // useEffect(() => {
  //   if (date) {
  //     setPress(false);
  //   }
  // }, [date]);

  // const waitDate = (calendarDate, result) => {
  //   return new Promise((resolve, reject) => {
  //     setdate(calendarDate);
  //     setTimeout(() => {
  //       resolve();
  //     }, 500);
  //   });
  // };

  return (
    <SafeAreaView>
      <HeaderTab titleCalender="Calender" page={goHome} calender={true} />

      <View>
        <Calendar
          disableAllTouchEventsForDisabledDays
          firstDay={1}
          minDate={new Date()}
          theme={{
            arrowColor: COLORS.black,
            selectedDayBackgroundColor: COLORS.red,
            selectedDayTextColor: COLORS.red,
            selectedDotColor: COLORS.red,
            todayTextColor: COLORS.black,
          }}
          markedDates={{
            [date]: {
              selected: true,
              disableTouchEvent: true,
              selectedColor: COLORS.white,
              selectedTextColor: COLORS.black,
              selectedDotColor: COLORS.red,
              marked: true,
              activeOpacity: 0,
            },
          }}
          onDayPress={day => {
            dispatch(
              getDataCalender({
                data: moment(day.dateString).format('DD/MM/YYYY'),
                dateName: moment(day.dateString).format('DD/MM/YYYY'),
              }),
            );
            setdate(day.dateString);
          }}
        />
      </View>
    </SafeAreaView>
  );
}
