import {StyleSheet} from 'react-native';
import {BorderlessButton} from 'react-native-gesture-handler';
import {moderateScale} from 'react-native-size-matters';
import {COLORS} from '../../Utils/Color';

const styles = StyleSheet.create({
  subtitlepagetext: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: moderateScale(20),
    // backgroundColor: 'yellow',
  },
  subtitlesignin: {
    fontSize: 12,
    textAlign: 'center',
    color: '#9098B1',
    marginBottom: 20,
  },
  orcont: {
    color: 'gray',
    padding: moderateScale(30),
    fontFamily: 'Inter-Bold',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  ortext: {
    color: COLORS.softBlack,
    fontFamily: 'Inter-Bold',
    alignItems: 'center',
    fontSize: moderateScale(18),
  },
  contuserreg: {
    // backgroundColor: 'yellow',
  },
  foottext: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  signtext: {
    fontSize: 12,
    color: COLORS.red,
    marginBottom: 20,
    alignItems: 'center',
  },
  conttitletext: {
    alignItems: 'center',
    marginTop: moderateScale(40),
    // backgroundColor: 'aqua',
  },
  hostertitletext: {
    fontSize: 20,
    alignItems: 'center',
    padding: moderateScale(10),
    // backgroundColor: 'yellow',
  },
  steptext: {
    color: COLORS.red,
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: moderateScale(20),
    marginTop: moderateScale(20),
    marginLeft: moderateScale(10),
  },
  bottomreg: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  uploadtext: {
    color: COLORS.black,
    fontSize: 13,
    fontWeight: 'bold',
    marginLeft: moderateScale(10),
    marginBottom: moderateScale(5),
    marginTop: moderateScale(10),
  },
  contenttext: {
    color: 'gray',
    fontSize: 12,
    marginLeft: moderateScale(10),
  },
  contfileupload: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: moderateScale(20),
    // backgroundColor: 'green',
  },
  fileicon: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: COLORS.softBlack,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    width: moderateScale(50),
    height: moderateScale(50),
    // backgroundColor: 'yellow',
  },
  filetext: {
    // borderWidth: 1,
    // alignItems: 'center',
    justifyContent: 'center',
    height: moderateScale(50),
    right: 80,
    // backgroundColor: 'aqua',
  },
  filetextbutab: {
    justifyContent: 'center',
    height: moderateScale(50),
    right: moderateScale(45),
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    // position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
    borderWidth: 0,
    // backgroundColor: 'yellow',
  },
  itemText: {
    fontSize: 12,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  },
  infoText: {
    textAlign: 'center',
    fontSize: 16,
  },
  searchSection: {
    flex: 1,
    height: 50,
    borderRadius: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: moderateScale(8),
    // backgroundColor: 'aqua',
    borderWidth: 1,
    borderColor: COLORS.softBlack,
    marginVertical: moderateScale(15),
  },
  searchIcon: {
    paddingLeft: 10,
    backgroundColor: 'transparent',
  },
  clearIcon: {
    paddingRight: 10,
    backgroundColor: 'transparent',
  },
  containerautocomplete: {
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: 'yellow',
  },
  autocompletecontainerstyle: {
    flex: 1,
    height: 100,
    backgroundColor: 'aqua',
  },
});

export default styles;
