//import React in our code
import React, {useState, useEffect} from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';

//import all the components we are going to use
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

//import Autocomplete component
import Autocomplete from 'react-native-autocomplete-input';

const TestBottom = () => {
  const data = ['BRI', 'BNI', 'BCA', 'Mandiri', 'Bank Mega'];
  const [bank, setBank] = useState([]);

  return (
    <View style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.searchSection}>
          <AntDesign
            name="search1"
            size={18}
            color="gray"
            style={styles.searchIcon}
          />
          <Autocomplete
            autoCapitalize="none"
            autoCorrect={false}
            hideResults={true}
            containerStyle={styles.autocompleteContainer}
            inputContainerStyle={styles.inputContainer}
            //data to show in suggestion
            data={bank}
            //default value if you want to set something in input
            // defaultValue={
            //   JSON.stringify(selectedValue) === '{}'
            //     ? ''
            //     : selectedValue.title + selectedValue.id
            // }
            // onchange of the text changing the state of the query
            // which will trigger the findFilm method
            // to show the suggestions
            onChangeText={text => setBank(text)}
            placeholder="Search doctors, specialities, symptoms"
            // renderItem={({item}) => (
            //   //you can change the view you want to show in suggestions
            //   <View>
            //     <TouchableOpacity
            //       onPress={() => {
            //         setSelectedValue(item);
            //         setFilteredFilms([]);
            //       }}>
            //       <Text style={styles.itemText}>{item.title + item.id}</Text>
            //     </TouchableOpacity>
            //   </View>
            // )}
            flatListProps={{
              keyExtractor: (_, idx) => idx,
              renderItem: ({item}) => <Text>{item}</Text>,
            }}
          />
          <AntDesign
            name="close"
            size={18}
            color="gray"
            style={styles.clearIcon}
          />
        </View>
        {/* <View style={styles.descriptionContainer}>
          {films.length > 0 ? (
            <>
              <Text style={styles.infoText}>Selected Data</Text>
              <Text style={styles.infoText}>
                {JSON.stringify(selectedValue)}
              </Text>
            </>
          ) : (
            <Text style={styles.infoText}>Enter The Film Title</Text>
          )}
        </View> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'yellow',
    flex: 1,
    padding: 16,
    marginTop: 40,
    borderWidth: 1,
  },
  autocompleteContainer: {
    backgroundColor: 'white',
    borderWidth: 1,
    marginLeft: 10,
    marginRight: 10,
    //paddingLeft: 15,
  },
  inputContainer: {
    //minWidth: 300,
    //width: "90%",
    //height: 55,
    backgroundColor: 'transparent',
    //color: '#6C6363',
    //fontSize: 18,
    //fontFamily: 'Roboto',
    borderBottomWidth: 1,
    //borderBottomColor: 'rgba(108, 99, 99, .7)',
    borderColor: 'transparent',
    borderWidth: 1,
  },
  descriptionContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  itemText: {
    fontSize: 15,
    paddingTop: 5,
    paddingBottom: 5,
    margin: 2,
  },
  infoText: {
    textAlign: 'center',
    fontSize: 16,
  },

  // testing below
  searchSection: {
    height: 50,
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
  },
  searchIcon: {
    //padding: 10,
    paddingLeft: 10,
    backgroundColor: 'transparent',
  },
  clearIcon: {
    paddingRight: 10,
    backgroundColor: 'transparent',
  },
});
export default TestBottom;
