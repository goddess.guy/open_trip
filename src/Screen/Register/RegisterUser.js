import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {Snackbar, HelperText} from 'react-native-paper';

//import from pages
import styles from './Styles';
//import from component
import Logo from '../../Components/Logo';
import AuthButton from '../../Components/AuthButton';
//import from utils
import {COLORS} from '../../Utils/Color';
import {PostNewUser, SetNewUserFailed} from './Redux/User/ActionUserRegister';
import {actionFailed} from '../../Redux/GlobalAction';
import LoadingAnimated from '../../Components/Loading';
import {heightPercentageToDP} from 'react-native-responsive-screen';

const RegisterUser = props => {
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordagain, setPasswordAgain] = useState('');
  const [message, setMessage] = useState('');
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const dataFailed = useSelector(state => state.UserRegister.message);
  const failed = useSelector(state => state.Global.failed);
  const loading = useSelector(state => state.Global.loading);
  // const loading = true;

  const hasErrors = (check, keyword) => {
    let arr = check.split('');
    if (arr.length === 0) {
      return false;
    } else if (arr.includes(keyword)) {
      return false;
    } else {
      return true;
    }
  };

  const passwordErr = (check, keyword) => {
    if (check !== keyword) {
      return true;
    } else {
      false;
    }
  };

  const submit = () => {
    if (!username) {
      setMessage('FullName must be field');
    } else if (!email) {
      setMessage('Email Must be field !!');
    } else if (!password) {
      setMessage('Password Must be field!!');
    } else if (!passwordagain) {
      setMessage('Password Must be field!!');
    } else if (passwordagain !== password) {
      setMessage('password must be the same');
    } else {
      dispatch(
        PostNewUser({
          username,
          email,
          password,
        }),
      );
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {loading ? (
        <LoadingAnimated />
      ) : (
        <ScrollView keyboardShouldPersistTaps="always">
          <View>
            <View style={{alignItems: 'center'}}>
              <Logo />
              <Text style={styles.subtitlepagetext}>Welcome to Open Trip</Text>
              <Text style={styles.subtitlesignin}>Sign Up to continue</Text>
            </View>
            <View style={styles.contuserreg}>
              <Input
                containerStyle={{height: heightPercentageToDP(10)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setUserName(text)}
                placeholder="Full Name"
                leftIcon={<Icon name="user" size={20} color="grey" />}
                style={{fontSize: 13}}
              />

              <Input
                containerStyle={{height: heightPercentageToDP(7)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setEmail(text)}
                placeholder="Your Email"
                leftIcon={<Icon name="mail" size={20} color="grey" />}
                style={{fontSize: 13}}
              />
              <HelperText type="error" visible={hasErrors(email, '@')}>
                email address is invalid
              </HelperText>
              <Input
                containerStyle={{height: heightPercentageToDP(10)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setPassword(text)}
                placeholder="Password"
                secureTextEntry
                leftIcon={<Icon name="lock1" fontSize size={20} color="grey" />}
                style={{fontSize: 13}}
              />

              <Input
                containerStyle={{height: heightPercentageToDP(7)}}
                inputContainerStyle={{
                  borderWidth: 1,
                  paddingLeft: moderateScale(15),
                  borderRadius: 5,
                  borderColor: COLORS.softBlack,
                }}
                onChangeText={text => setPasswordAgain(text)}
                placeholder="Password Again"
                secureTextEntry
                leftIcon={<Icon name="lock1" size={20} color="grey" />}
                style={{fontSize: 13}}
              />
              <HelperText
                type="error"
                visible={passwordErr(password, passwordagain)}>
                {message}
              </HelperText>
              <AuthButton
                center
                full
                judul="Sign Up"
                submit={submit}
                fullContainerAdd={{marginBottom: moderateScale(10)}}
              />
              <View style={styles.foottext}>
                <Text style={styles.subtitlesignin}>Have an account? </Text>
                <TouchableOpacity
                  onPress={() => props.navigation.navigate('LoginScreen')}>
                  <Text style={styles.signtext}>Sign In</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      )}
      <Snackbar
        style={{backgroundColor: COLORS.red}}
        visible={failed}
        onDismiss={() => {
          dispatch(actionFailed(false));
          dispatch(SetNewUserFailed(''));
        }}
        duration={3000}>
        {dataFailed}
      </Snackbar>
    </SafeAreaView>
  );
};

export default RegisterUser;
