import React from 'react';
import {StyleSheet, Text, View, SafeAreaView} from 'react-native';

//import from component
import Logo from '../../Components/Logo';
import AuthButton from '../../Components/AuthButton';
//import from assets
import Line from '../../Assets/Images/LineReg.svg';
//import from pages
import styles from './Styles';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

const Register = props => {
  const keUser = () => {
    props.navigation.navigate('RegisterUser');
  };
  const keHost = () => {
    props.navigation.navigate('RegisterHoster1');
  };

  return (
    <SafeAreaView>
      <View>
        <View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Logo />
            <Text style={styles.subtitlepagetext}>Choose Registrations</Text>
          </View>
          <View style={{marginTop: moderateScale(30)}}>
            <AuthButton submit={keUser} judul="Register as User" />
            <View style={styles.orcont}>
              <Line />
              <Text style={styles.ortext}>OR</Text>
              <Line />
            </View>

            <AuthButton submit={keHost} judul="Register as Hoster" />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default Register;
