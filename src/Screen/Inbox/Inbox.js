import React, {useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  FlatList,
  StyleSheet,
  LogBox,
} from 'react-native';
import HeaderTab from '../../Components/Header';
import {INBOXDUMMY} from '../../Utils/DummyData';

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import moment from 'moment';
import {moderateScale} from 'react-native-size-matters';
import {COLORS} from '../../Utils/Color';
import Inter from '../../Components/Inter';
import {widthPercentageToDP} from 'react-native-responsive-screen';

const Inbox = () => {
  const data = INBOXDUMMY;

  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);

  return (
    <SafeAreaView style={styles.safeView}>
      <HeaderTab title="Inbox" arrow={false} />
      <ScrollView>
        <View>
          <FlatList
            data={data}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity key={index.toString()} activeOpacity={0.7}>
                  <View style={styles.inboxContainer}>
                    <MaterialCommunityIcons
                      name="inbox"
                      size={moderateScale(24)}
                      color={COLORS.red}
                      style={{paddingLeft: moderateScale(5)}}
                    />
                    <View style={{paddingHorizontal: moderateScale(10)}}>
                      <View style={styles.contentContainer}>
                        <Inter type="Bold" text={item.title} />
                        <Inter
                          size={moderateScale(10)}
                          text={moment(item.time).format('DD MMM YYYY, h:mm a')}
                        />
                      </View>
                      <View
                        style={{
                          paddingRight: moderateScale(10),
                          width: widthPercentageToDP(85),
                        }}>
                        <Inter
                          size={moderateScale(12)}
                          text={item.message}
                          style={styles.textJustify}
                          line={2}
                          ellis="tail"
                        />
                        {/* <Text
                        style={{
                          fontFamily: 'Inter',
                          fontSize: moderateScale(12),
                          color: COLORS.softBlack,
                        }}
                        numberOfLines={2}
                        ellipsizeMode="tail">
                        {item.message}
                      </Text> */}
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Inbox;

const styles = StyleSheet.create({
  safeView: {
    flex: 1,
  },
  inboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: widthPercentageToDP(100),
    padding: moderateScale(10),
    borderBottomWidth: moderateScale(1),
    borderBottomColor: COLORS.softBlack,
  },
  contentContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: moderateScale(10),
  },
  textJustify: {
    textAlign: 'justify',
  },
});
