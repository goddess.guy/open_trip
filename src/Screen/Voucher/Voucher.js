import React from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {COLORS} from '../../Utils/Color';
import Logo from '../../Components/Logo';
import Inter from '../../Components/Inter';
import AuthButton from '../../Components/AuthButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector, useDispatch} from 'react-redux';
import moment from 'moment';
import ErrorScreen from '../../Components/Error';
import LoadingAnimated from '../../Components/Loading';
const Voucher = props => {
  const dataVoucher = useSelector(state => state.Profile?.voucher);
  const Loading = useSelector(state => state.Global.loading);
  const userdata = useSelector(state => state.Login?.userData);
  const dataOrder = useSelector(state => state.Order?.order?.orders);

  return !dataOrder ? (
    <ErrorScreen />
  ) : (
    <SafeAreaView style={{flex: 1}}>
      {Loading ? (
        <LoadingAnimated />
      ) : (
        <>
          <View>
            <Logo />
          </View>
          <View
            style={{
              alignContent: 'center',
              alignItems: 'center',
              marginBottom: 20,
            }}>
            <Inter
              text="Thankyou! Your payment has been confirmed "
              size={moderateScale(13)}
              color={COLORS.red}
            />
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
            }}>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View style={styles.status}>
                <Icon
                  name="ticket-confirmation"
                  size={30}
                  color={COLORS.softBlack}
                />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>
                    Booking No
                  </Text>
                  <Text style={{fontSize: 12, color: COLORS.softBlack}}>
                    {dataVoucher.orders.order_no}
                  </Text>
                </View>
              </View>
              <View style={styles.status}>
                <Icon name="calendar" size={30} color={COLORS.softBlack} />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>Date</Text>
                  <Text style={{fontSize: 12, color: COLORS.softBlack}}>
                    {moment(dataOrder.updateAt).format('DD MMMM YYYY')}
                  </Text>
                </View>
              </View>
              <View style={styles.status}>
                <Icon name="email" size={30} color={COLORS.softBlack} />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>Email</Text>
                  <Text style={{fontSize: 12, color: COLORS.softBlack}}>
                    {userdata.email}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={{
                marginHorizontal: 10,
              }}>
              <View style={styles.status}>
                <Icon name="cash" size={30} color={COLORS.softBlack} />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>Total</Text>
                  <Text style={{fontSize: 12, color: COLORS.softBlack}}>
                    {`Rp. ${new Intl.NumberFormat(['ban', 'id']).format(
                      dataVoucher.orders.total_price,
                    )}`}
                  </Text>
                </View>
              </View>
              <View style={styles.status}>
                <Icon name="account-cash" size={30} color={COLORS.softBlack} />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>
                    Payment
                  </Text>
                  <Text style={{fontSize: 12, color: COLORS.softBlack}}>
                    Bank Transfer
                  </Text>
                </View>
              </View>
              <View style={styles.status}>
                <Icon
                  name="check-box-outline"
                  size={30}
                  color={COLORS.softBlack}
                />
                <View style={{marginHorizontal: 15}}>
                  <Text style={{fontWeight: 'bold', fontSize: 15}}>Status</Text>
                  <Text
                    style={{
                      fontSize: 12,
                      color: COLORS.softBlack,
                    }}>
                    {dataVoucher.orders.payment_status}
                  </Text>
                </View>
              </View>
            </View>
          </View>

          <View style={styles.status}>
            <View style={styles.contbackground}>
              <View style={styles.contticket}>
                <View
                  style={{
                    marginTop: moderateScale(80),
                    right: moderateScale(30),
                    transform: [{rotate: '90deg'}],
                    position: 'absolute',
                  }}>
                  <Text style={{color: 'white'}}>
                    {dataVoucher.orders.voucher_code}
                  </Text>
                </View>
                <View style={styles.halfcircleleft} />
                <View style={styles.halfcircleright} />

                <View
                  style={{
                    flex: 1,
                  }}>
                  <Text
                    style={{
                      color: 'white',
                      fontSize: moderateScale(25),
                      fontWeight: 'bold',

                      paddingHorizontal: moderateScale(35),
                      paddingTop: moderateScale(12),
                    }}>
                    E - VOUCHER
                  </Text>

                  <Text
                    style={{
                      color: 'white',
                      fontSize: moderateScale(15),
                      fontWeight: 'bold',
                      paddingHorizontal: moderateScale(35),
                      fontStyle: 'italic',
                    }}>
                    {dataVoucher.trip_name}
                  </Text>
                  <View>
                    <Text
                      style={{
                        color: 'white',
                        paddingHorizontal: moderateScale(35),
                        paddingTop: moderateScale(13),
                        paddingBottom: moderateScale(5),
                      }}>
                      Tanggal : {dataVoucher.orders.date_order}
                    </Text>
                    <Text
                      style={{
                        color: 'white',
                        paddingHorizontal: moderateScale(35),
                      }}>
                      Durasi : {`${dataVoucher.duration_trip} Hari`}
                    </Text>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: 'bold',
                        fontSize: moderateScale(18),
                        paddingHorizontal: moderateScale(35),
                        paddingVertical: moderateScale(15),
                      }}>
                      [ {userdata.username} ]
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              marginTop: moderateScale(20),
              alignItems: 'center',
            }}>
            <AuthButton
              submit={() => props.navigation.navigate('Profile')}
              mini
              judul="Back"
            />
          </View>
        </>
      )}
    </SafeAreaView>
  );
};

export default Voucher;

const styles = StyleSheet.create({
  contbackground: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    // marginTop: moderateScale(25),
  },
  contticket: {
    width: moderateScale(350),
    backgroundColor: COLORS.red,
    height: moderateScale(200),
    borderRadius: 10,
    elevation: 8,
  },
  halfcircleleft: {
    position: 'absolute',
    width: moderateScale(110),
    height: moderateScale(100),
    backgroundColor: 'transparent',
    borderRadius: 50,
    left: -80,
    top: moderateScale(50),
  },
  halfcircleright: {
    position: 'absolute',
    width: moderateScale(110),
    height: moderateScale(100),
    backgroundColor: 'white',
    borderRadius: 50,
    right: -80,
    top: moderateScale(50),
  },
  contstatus: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  status: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    elevation: 10,
  },
});
