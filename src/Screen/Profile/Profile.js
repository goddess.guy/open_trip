import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  SafeAreaView,
} from 'react-native';

import {moderateScale} from 'react-native-size-matters';

import AuthButton from '../../Components/AuthButton';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Snackbar} from 'react-native-paper';
import {COLORS} from '../../Utils/Color';

import {useSelector, useDispatch} from 'react-redux';
import {logoutAction} from '../Login/Redux/ActionLogin';
import {navigate} from '../../Function/nav';
import {actionSuccess, actionFailed} from '../../Redux/GlobalAction';
import {getOrderId} from '../Order/Redux/ActionOrder';
import {setOrderIdFailed, SetProfileFailed} from './Redux/ActionProfile';
import LoadingAnimated from '../../Components/Loading';

const Profile = props => {
  const dispatch = useDispatch();

  const userdata = useSelector(state => state.Login?.userData);

  const idOrder = useSelector(state => state.Order?.order?.orders?.id);

  const photo = useSelector(state => state.Profile.foto);
  const success = useSelector(state => state.Global.Success);
  const loading = useSelector(state => state.Global.loading);
  const failed = useSelector(state => state.Global.failed);
  const messageSuccess = useSelector(state => state.Login?.message);
  const messageFailed = useSelector(state => state.Profile?.message);

  return (
    <SafeAreaView style={styles.container}>
      {loading ? (
        <LoadingAnimated />
      ) : (
        <>
          <View style={styles.header}>
            <View
              style={{alignItems: 'center', marginBottom: moderateScale(25)}}>
              <TouchableOpacity onPress={() => {}}>
                <View
                  style={{
                    height: 100,
                    width: 100,
                    borderRadius: 15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <ImageBackground
                    source={
                      !photo
                        ? require('../../Assets/Images/luffy.jpg')
                        : {uri: photo.uri}
                    }
                    style={{height: 100, width: 100}}
                    imageStyle={{borderRadius: 15}}></ImageBackground>
                </View>
              </TouchableOpacity>
              <View>
                <Text
                  style={{
                    fontWeight: 'bold',
                    color: '#fff',
                    marginTop: moderateScale(10),
                    fontSize: 18,
                  }}>
                  Hello {userdata ? userdata.username : ''}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.footer}>
            <View
              style={{
                justifyContent: 'space-between',
                paddingHorizontal: moderateScale(2),
                paddingTop: moderateScale(25),
              }}>
              {/* edit profile  */}
              <TouchableOpacity
                onPress={() =>
                  props.navigation.navigate('EditProfile', userdata)
                }>
                <View style={styles.contfileupload}>
                  <View style={styles.fileicon}>
                    <Icon name="account" size={25} color="grey" />
                  </View>
                  <View style={styles.filetext}>
                    <Text style={{fontWeight: 'bold'}}>Edit Profile</Text>
                    <Text style={{color: COLORS.softBlack}}>Click to edit</Text>
                  </View>
                  <Icon name="chevron-right" size={15} color="grey" />
                </View>
              </TouchableOpacity>
              {/* Voucher */}
              <TouchableOpacity
                onPress={() => (!idOrder ? {} : dispatch(getOrderId(idOrder)))}>
                <View style={styles.contfileupload}>
                  <View style={styles.fileicon}>
                    <Icon name="barcode-scan" size={25} color="grey" />
                  </View>
                  <View style={styles.filetext}>
                    <Text style={{fontWeight: 'bold'}}>Vouchers</Text>
                    <Text style={{color: COLORS.softBlack}}>Click to See</Text>
                  </View>
                  <Icon name="chevron-right" size={15} color="grey" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => (!idOrder ? {} : navigate('OrderReceived'))}>
                <View style={styles.contfileupload}>
                  <View style={styles.fileicon}>
                    <Icon name="layers-search" size={25} color="grey" />
                  </View>
                  <View style={styles.filetext}>
                    <Text style={{fontWeight: 'bold'}}>Booking List</Text>
                    <Text style={{color: COLORS.softBlack}}>Check </Text>
                  </View>
                  <Icon name="chevron-right" size={15} color="grey" />
                </View>
              </TouchableOpacity>
            </View>
            <Snackbar
              style={{backgroundColor: COLORS.red}}
              visible={failed ? failed : success ? success : false}
              onDismiss={() => {
                dispatch(actionFailed(false));
                dispatch(actionSuccess(false));
                dispatch(SetProfileFailed(''));
                dispatch(setOrderIdFailed(''));
              }}
              duration={2000}>
              {failed ? messageFailed : messageSuccess}
            </Snackbar>
            <View>
              <View style={styles.bottomreg}>
                <AuthButton
                  submit={() => {
                    navigate('LoginScreen');
                    dispatch(logoutAction());
                    dispatch(actionSuccess(false));
                    dispatch(actionFailed(false));
                  }}
                  // style={{width: moderateScale(150), height: moderateScale(50)}}
                  full
                  judul="Log Out"
                />
              </View>
            </View>
          </View>
        </>
      )}
    </SafeAreaView>
  );
};

export default Profile;
