export const PUT_PROFILE = 'PUT_PROFILE';
export const SET_PROFILE_SUCCESS = 'SET_PROFILE_SUCCESS';
export const SET_PROFILE_FAILED = 'SET_PROFILE_FAILED';
export const SET_PHOTO = 'SET_PHOTO';
export const SET_ORDER_ID_SUCCESS = 'SET_ORDER_ID_SUCCESS';
export const SET_ORDER_ID_FAILED = 'SET_ORDER_ID_FAILED';

export const PutProfile = payload => {
  return {type: PUT_PROFILE, payload};
};

export const SetProfileSuccess = payload => {
  return {type: SET_PROFILE_SUCCESS, payload};
};

export const SetProfileFailed = payload => {
  return {type: SET_PROFILE_FAILED, payload};
};

export const setPhoto = payload => {
  return {type: SET_PHOTO, payload};
};

export const setOrderIdSuccess = payload => ({
  type: SET_ORDER_ID_SUCCESS,
  payload,
});
export const setOrderIdFailed = payload => ({
  type: SET_ORDER_ID_FAILED,
  payload,
});
