import {put, takeLatest} from 'redux-saga/effects';
import {navigate} from '../../../Function/nav';
import {
  actionLoading,
  actionSuccess,
  actionFailed,
} from '../../../Redux/GlobalAction';
import {openTrip} from '../../../Utils/baseurl';
import {
  PUT_PROFILE,
  SetProfileFailed,
  SetProfileSuccess,
} from './ActionProfile';

function* updateProfile(action) {
  try {
    yield put(actionLoading(true));
    const {id, username, password, email, token} = action.payload;

    const body = {
      username: username,
      email: email,
      password: password,
    };

    const res = yield openTrip({
      url: `travel/upd?id=${id}`,
      method: 'PUT',
      data: body,
      headers: {Authorization: token},
    });

    // const res = yield axios.put(url + `travel/upd?id=${id}`, body, {
    //   headers: {Authorization: token},
    // });
    yield console.log(res, 'update Profile');
    if (res) {
      yield put(actionSuccess(true));
      yield put(SetProfileSuccess(res.data));
      yield navigate('Profile');
      yield put(actionLoading(false));
    }
  } catch (error) {
    yield console.log(error);
    yield put(actionFailed(true));
    yield put(SetProfileFailed(error.response.data.message));
    yield put(actionLoading(false));
  }
}

export function* SagaProfile() {
  yield takeLatest(PUT_PROFILE, updateProfile);
}
