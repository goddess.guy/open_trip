// react
import React from 'react';

// navigation
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

// component
import Profile from '../../Screen/Profile/Profile';
import Trip from '../../Screen/Trip/Trip';
import Inbox from '../../Screen/Inbox/Inbox';
import HomeRoute from '../../Routers/HomeRoute';

// library
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import ProfileRoute from '../../Routers/ProfileRoute';

const Tab = createMaterialBottomTabNavigator();

const BottomTab = () => {
  return (
    <>
      <Tab.Navigator
        initialRouteName="Explore"
        activeColor="#F24B5B"
        inactiveColor="grey"
        shifting={false}
        barStyle={{backgroundColor: '#ffffff'}}>
        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <MaterialIcons
                name="explore"
                size={23}
                color={color}
                style={{transform: [{rotateY: '180deg'}]}}
              />
            ),
          }}
          name="Explore"
          component={HomeRoute}
        />
        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <Entypo name="suitcase" size={21} color={color} />
            ),
          }}
          name="Trip"
          component={Trip}
        />
        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <MaterialIcons name="mail" size={23} color={color} />
            ),
          }}
          name="Inbox"
          component={Inbox}
        />
        <Tab.Screen
          options={{
            tabBarIcon: ({color}) => (
              <Ionicons name="ios-person-circle" size={24} color={color} />
            ),
          }}
          name="Profile"
          component={ProfileRoute}
        />
      </Tab.Navigator>
    </>
  );
};

export default BottomTab;
