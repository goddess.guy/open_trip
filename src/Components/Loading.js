import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {ActivityIndicator} from 'react-native-paper';
import {useDispatch} from 'react-redux';
import {actionLoading} from '../Redux/GlobalAction';
import Lottie from 'lottie-react-native';

import {COLORS} from '../Utils/Color';
import {moderateScale} from 'react-native-size-matters';
import Inter from './Inter';
import {widthPercentageToDP} from 'react-native-responsive-screen';

const LoadingAnimated = () => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: COLORS.white,
    },
    containerText: {
      paddingHorizontal: moderateScale(90),
    },
  });

  const pantun = [
    `Sungguh luas alam dunia,
Pergi ke kebun petik petai.
Siapa tau ada yang tanya,
Saya lagi liburan di pantai.`,
    `Hari raya makan ketupat,
Ketupat lezat rasanya.
Berenang-renang ke Raja Ampat,
Tempat wisata kelas dunia.`,
    `Burung gagak turun ke sebuah rawa,
takut ditangkap oleh Sang raja.
Selamat Liburan untuk semua,
Mari bersenang-senang jangan bekerja.`,
    `Makanan Empek-empek tambah cuka,
Lezat sekali dirasa oleh lidah
Ke kota aku sangat suka,
Ke desa juga banyak yang indah`,
    `Pasar baru tempatnya jualan kain,
Awas dompet jangan sampai hilang
Desa itu tempatnya bermain,
Mari berlibur Seperti bolang berpetualang`,
    `Bunga cantik namanya seroja,
Hewan cantik itu namanya rusa
Liburan ke mana saja,
Pilih Pergi ke kota atau ke desa?`,
    `Ulat bulu panjang sekilan,
kera beruk kakinya di rantai.
Hari sabtu jalan-jalan,
mau gunung atau ke pantai?`,
  ];

  const dispatch = useDispatch();

  const randomPantun = pantun[Math.floor(Math.random() * pantun.length)];

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => dispatch(actionLoading(false))}>
        {/* <ActivityIndicator animating={true} color={COLORS.red} />
         */}
        <Lottie
          source={require('../Assets/Images/loading.json')}
          autoPlay
          loop
          resizeMode="contain"
          style={{
            height: moderateScale(100),
          }}
        />

        <Inter
          style={{textAlign: 'center', letterSpacing: 0.3}}
          text={randomPantun}
          size={moderateScale(12)}
          type="Medium"
        />
      </TouchableOpacity>
      {/* <Text style={{letterSpacing}}></Text> */}
    </View>
  );
};

export default LoadingAnimated;

// import React from 'react';
// import {View} from 'react-native';
// import {ActivityIndicator} from 'react-native-paper';

// export default function LoadingAnimated() {
//   return (
//     <View>
//       <ActivityIndicator color="blue" size={100} />
//     </View>
//   );
// }
