import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {COLORS} from '../Utils/Color';

const Inter = props => {
  const styles = StyleSheet.create({
    textStyle: {
      fontFamily: `Inter-${props.type ? props.type : 'Regular'}`,
      fontSize: props.size ? props.size : moderateScale(16),
      color: props.color ? props.color : COLORS.black,
      ...props.style,
    },
    textStyle1: {
      fontFamily: `Inter-${props.type1 ? props.type1 : 'Regular'}`,
      fontSize: props.size1 ? props.size1 : moderateScale(16),
      color: props.color1 ? props.color1 : COLORS.black,
      ...props.style1,
    },
  });
  return (
    <Text
      numberOfLines={props.line}
      ellipsizeMode={props.ellip}
      style={styles.textStyle}>
      {props.text}
      <Text style={styles.textStyle1}>{props.text1}</Text>
    </Text>
  );
};

export default Inter;
