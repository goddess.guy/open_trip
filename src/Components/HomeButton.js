// react
import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';

// library
import {moderateScale} from 'react-native-size-matters';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const HomeButton = props => {
  const styles = StyleSheet.create({
    button: {
      borderRadius: 5,
      width: widthPercentageToDP(27),
      // flexWrap: 'wrap',
      height: moderateScale(35),
      marginRight: moderateScale(8),
      flexDirection: 'row',
      alignItems: 'center',
      alignContent: 'space-around',
      justifyContent: 'center',
      borderWidth: 1.5,
      borderColor: props.warna,
    },

    textButton: {
      color: '#F14F6F',
      fontSize: moderateScale(12),
      alignSelf: 'center',
      fontWeight: 'bold',
    },
    textButton1: {
      color: props.text,
      fontSize: moderateScale(11),
      alignSelf: 'center',
    },
  });
  return (
    <View>
      <View style={styles.button}>
        <TouchableOpacity
          onPress={props.kalender ? props.kalender : props.month}>
          <Text style={styles.textButton}>{props.name}</Text>
        </TouchableOpacity>

        <Text style={styles.textButton1}>{props.filter}</Text>
      </View>
    </View>
  );
};

export default HomeButton;
