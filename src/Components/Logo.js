import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
//import from Assets
import OPLogo from '../Assets/Images/OPLogo.svg';
import CompLogo2 from '../Assets/Images/iconcircle.svg';

export default function Logo(props) {
  return (
    <View style={styles.logo}>
      <OPLogo />
      <CompLogo2 marginLeft={10} />
    </View>
  );
}

const styles = StyleSheet.create({
  logo: {
    flex: 1,
    flexDirection: 'row',
    padding: 85,
    alignItems: 'center',
    paddingTop: 150,
    paddingBottom: 30,
  },
});
