import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import ErrorImage from '../Assets/Images/error.svg';
import Inter from '../Components/Inter';
import {COLORS} from '../Utils/Color';
import Lottie from 'lottie-react-native';

export default function ErrorScreen({judul = 'Sorry, something went wrong.'}) {
  return (
    <View style={styles.container}>
      {/* <ErrorImage width={moderateScale(250)} height={moderateScale(250)} /> */}
      <Lottie
        style={{width: moderateScale(400)}}
        resizeMode="cover"
        autoPlay
        loop
        source={require('../Assets/Images/lf30_editor_bkakehfh.json')}
      />
      <Inter text={judul} color={COLORS.red} type="Bold" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
