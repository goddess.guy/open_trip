// react
import React, {useEffect} from 'react';

// Navigation
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Screen

import SplashScreen from 'react-native-splash-screen';
import Login from '../Screen/Login/Login';
import ForgotPassword from '../Screen/Login/ForgotPassword';
import BottomTab from '../Components/Bottom/BottomTab';
import Register from '../Screen/Register/Register';
import RegisterUser from '../Screen/Register/RegisterUser';
import Voucher from '../Screen/Voucher/Voucher';
import RegisterHoster1 from '../Screen/Register/RegisterHoster1';
import RegisterHoster2 from '../Screen/Register/RegisterHoster2';
import {navigate, navigationRef} from '../Function/nav';
import {useSelector, useDispatch} from 'react-redux';
import Error from '../Components/Error';
import {
  actionExpired,
  actionIsLogged,
  actionFailed,
} from '../Redux/GlobalAction';
import moment from 'moment';
import {loginFailed} from '../Screen/Login/Redux/ActionLogin';
// nav saga

const Stack = createStackNavigator();
const myTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: '#ffffff',
  },
};

export default function Main() {
  const dispatch = useDispatch();
  const DataLogin = useSelector(state => state.Global.isLogged);
  const TimeLogin = useSelector(state => state.Global.timeLogin);
  const TimeExp = useSelector(state => state.Global.timeExp);
  console.log(TimeExp, 'time expired');

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);

    if (moment().format() >= TimeExp) {
      dispatch(actionExpired(true));
      dispatch(actionIsLogged(false));
      dispatch(actionIsLogged(false));
      dispatch(loginFailed('Token Expired'));
      dispatch(actionFailed(true));
      navigate('LoginScreen');
    }
  }, [dispatch]);

  return (
    <NavigationContainer theme={myTheme} ref={navigationRef}>
      <Stack.Navigator initialRouteName={DataLogin ? 'Main' : 'LoginScreen'}>
        <Stack.Screen
          name="LoginScreen"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="ForgotPassword"
          component={ForgotPassword}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Main"
          component={BottomTab}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RegisterUser"
          component={RegisterUser}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RegisterHoster1"
          component={RegisterHoster1}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="RegisterHoster2"
          component={RegisterHoster2}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Error"
          component={Error}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Voucher"
          component={Voucher}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
