import React from 'react';
import {View, Text} from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import Home from '../Screen/Home/Home';
import HomeCalender from '../Screen/Home/HomeCalender/HomeCalender';
import DetailHome from '../Screen/Home/HomeDetail/DetailHome';
import OrderScreen from '../Screen/Order/OrderScreen';
import OrderReceivedScreen from '../Screen/Order/OrderReceived/OrderReceivedScreen';
import Calender from '../Screen/Calender/Calender';

const Stack = createStackNavigator();

const HomeRoute = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="HomeCalender"
        component={HomeCalender}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="HomeDetail"
        component={DetailHome}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Order"
        component={OrderScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="OrderReceived"
        component={OrderReceivedScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Calendar"
        component={Calender}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default HomeRoute;
