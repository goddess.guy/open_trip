import React from 'react';
import {View, Text} from 'react-native';

import {createStackNavigator} from '@react-navigation/stack';
import Profile from '../Screen/Profile/Profile';
import EditProfile from '../Screen/Profile/EditProfile';

import Voucher from '../Screen/Voucher/Voucher';

const Stack = createStackNavigator();

const ProfileRoute = () => {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Voucher"
        component={Voucher}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default ProfileRoute;
