import {all} from 'redux-saga/effects';

//register
import {SagaUserRegister} from '../Screen/Register/Redux/User/SagaUserRegister';
import {SagaHosterRegister} from '../Screen/Register/Redux/Hoster/SagaHosterRegister';

// home
import SagaHome from '../Screen/Home/Redux/SagaHome';
import SagaHomeCalender from '../Screen/Home/HomeCalender/Redux/sagaHomeCalender';
import SagaCalender from '../Screen/Calender/Redux/sagaCalender';

// trip
import SagaSearchTrip from '../Screen/Trip/Redux/SagaSearchTrip';

//Login
import {SagaLogin} from '../Screen/Login/Redux/SagaLogin';
import SagaDetail from '../Screen/Home/HomeDetail/Redux/SagaDetailHome';
import {SagaProfile} from '../Screen/Profile/Redux/SagaProfile';
import {SagaOrder} from '../Screen/Order/Redux/SagaOrder';

function* SagaWatcher() {
  yield all([
    SagaUserRegister(),
    SagaHosterRegister(),
    SagaLogin(),
    SagaHome(),
    SagaHomeCalender(),
    SagaSearchTrip(),
    SagaDetail(),
    SagaProfile(),
    SagaCalender(),
    SagaOrder(),
  ]);
}

export default SagaWatcher;
