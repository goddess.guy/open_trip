import 'react-native-gesture-handler';
import 'intl';
import 'intl/locale-data/jsonp/en';
import React, {useEffect} from 'react';
import {LogBox} from 'react-native';
// Component
import Main from './src/Routers/Main';
import {Store, Persistor} from './src/Redux/Store';

// Redux
import {PersistGate} from 'redux-persist/lib/integration/react';
import {Provider} from 'react-redux';

export default function App() {
  useEffect(() => {
    LogBox.ignoreLogs(['Reanimated 2']);
    LogBox.ignoreAllLogs();
  }, []);

  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <Main />
      </PersistGate>
    </Provider>
  );
}
